﻿using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace DecaTec.WebDav.UnitTest
{
	[TestClass]
	public class UnitTestWebDavResponseMessage
	{
        [TestMethod]
        public void UT_WebDavResponseMessage_Transparently_Wrap_Headers()
        {
            const string expected = "Jetty(9.3.9.v20160517)";
            var responseToWrap = new HttpResponseMessage();
            responseToWrap.Headers.TryAddWithoutValidation("Server", expected);

            var wrapper = new WebDavResponseMessage(responseToWrap);
            var actual = wrapper.Headers.GetValues("Server").FirstOrDefault();

			Assert.AreEqual(expected, actual);
		}

        [TestMethod]
        [DataRow("\"test\"", "test")]
        [DataRow("\"test\"", "\"test\"")]
        [DataRow("W/\"test\"", "W/test")]
        [DataRow("W/\"test\"", "W/\"test\"")]
        [DataRow("\"\\\"\"", "\"")]
        [DataRow("W/\"\\\"\"", "W/\"")]
        public void UT_WebDavResponseMessage_Keep_Etag_Header(
	        string expected,
	        string etag)
        {
	        var responseToWrap = new HttpResponseMessage();
	        responseToWrap.Headers.TryAddWithoutValidation("ETag", etag);

	        var wrapper = new WebDavResponseMessage(responseToWrap);

	        Assert.IsNotNull(wrapper.Headers.ETag);
	        Assert.AreEqual(expected, wrapper.Headers.ETag.ToString());
        }
	}
}
