﻿using System;
using System.Net;
using System.Net.Http;
using System.Linq;

namespace DecaTec.WebDav
{
    /// <summary>
    /// Class representing a WebDAV response.
    /// </summary>
    public class WebDavResponseMessage : HttpResponseMessage
    {
        /// <summary>
        /// Initializes a new instance of WebDavResponseMessage.
        /// </summary>
        public WebDavResponseMessage()
        {

        }

        /// <summary>
        /// Initializes a new instance of WebDavResponseMessage.
        /// </summary>
        /// <param name="statusCode">The request's HttpStatusCode.</param>
        public WebDavResponseMessage(HttpStatusCode statusCode)
            : base(statusCode)
        {

        }

        /// <summary>
        /// Initializes a new instance of WebDavResponseMessage.
        /// </summary>
        /// <param name="statusCode">The request's <see cref="WebDavStatusCode"/>.</param>
        public WebDavResponseMessage(WebDavStatusCode statusCode)
            : base((HttpStatusCode)statusCode)
        {
        }

        /// <summary>
        /// Initializes a new instance of WebDavResponseMessage.
        /// </summary>
        /// <param name="httpResponseMessage">The <see cref="HttpResponseMessage"/> the WebDavResponseMessage should be based on.</param>
        public WebDavResponseMessage(HttpResponseMessage httpResponseMessage)
        {
            this.Content = httpResponseMessage.Content;
            this.ReasonPhrase = httpResponseMessage.ReasonPhrase;
            this.RequestMessage = httpResponseMessage.RequestMessage;
            this.StatusCode = (WebDavStatusCode)httpResponseMessage.StatusCode;
            this.Version = httpResponseMessage.Version;

            // Transfer headers.
            foreach (var header in httpResponseMessage.Headers)
            {
                if (header.Key == WebDavConstants.ETag && header.Value != null && header.Value.Any())
                {
                    // Workaround when server returns invalid header (e.g."686897696a7c876b7e" instead of "\"686897696a7c876b7e\"").
                    var fixedValues = header.Value
                        .Select(FixEntityTag)
                        .Where(x => !string.IsNullOrEmpty(x))
                        .ToList();
                    if (fixedValues.Count > 0)
                    {
                        Headers.Add(header.Key, fixedValues);
                    }
                }
                else
                {
                    // This wrapper is supposed to be transparent.
                    // Assume the headers in the httpResponseMessage are always valid
                    // otherwise they should not have been there in the first place.
                    this.Headers.TryAddWithoutValidation(header.Key, header.Value);
                }
			}
        }

        /// <summary>
        /// Gets or sets the <see cref="WebDavStatusCode"/> of this WebDavResponseMessage.
        /// </summary>
        public new WebDavStatusCode StatusCode
        {
            get => (WebDavStatusCode)base.StatusCode;
            set => base.StatusCode = (HttpStatusCode)value;
        }

        /// <summary>
        /// Fixes the entity tag for broken servers.
        /// </summary>
        /// <param name="entityTag">The entity tag to fix.</param>
        /// <returns>The fixed entity tag or <see langword="null"/> if it's beyond fixable.</returns>
        private static string FixEntityTag(string entityTag)
        {
            bool isWeak;
            string quotedString;
            if (entityTag.StartsWith("W/", StringComparison.OrdinalIgnoreCase))
            {
                // Extract the "weak" prefix
                isWeak = true;
                quotedString = entityTag.Substring(2);
            }
            else
            {
                isWeak = false;
                quotedString = entityTag;
            }

            // Ignore empty entry
            if (string.IsNullOrEmpty(quotedString))
            {
                return null;
            }

            if (quotedString.Length == 1 && quotedString[0] == '"')
            {
                // An entity tag may contain a quotation mark.
                quotedString = "\"\\\"\"";
            }
            else if (!quotedString.StartsWith("\"") && !quotedString.EndsWith("\""))
            {
                // No enclosing quotation marks were found - add ours!
                // MAYBE: Test for unescaped quotation marks?
                quotedString = "\"" + quotedString + "\"";
            }

            // Rebuild the entity tag.
            return isWeak
                ? "W/" + quotedString
                : quotedString;
        }
    }
}
